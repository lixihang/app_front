Page({
  data: {
    longitude: 113.14278, //地图界面中心的经度
    latitude: 23.02882, //地图界面中心的纬度
    markers: [ //标志点的位置
      //位置0
      {
        id: 0,
        iconPath: "../imgs/png1.png",
        latitude: 23.04308268848755,
        longitude: 113.13562655285273,
        width: 28,
        height: 32
      },
      //位置1
      {
        id: 1,
        iconPath: "../imgs/png1.png",
        latitude: 23.03814334110172,
        longitude: 113.15378758608608,
        width: 28,
        height: 32
      },
      //位置2
      {
        id: 2,
        iconPath: "../imgs/png1.png",
        latitude: 23.03377135768493,
        longitude: 113.17075479351229,
        width: 28,
        height: 32
      },
      //位置3
      {
        id: 3,
        iconPath: "../imgs/png1.png",
        latitude: 23.02542516569787,
        longitude: 113.1417505798413,
        width: 28,
        height: 32
      },
      //位置4
      {
        id: 4,
        iconPath: "../imgs/png1.png",
        latitude: 23.022740816325694,
        longitude: 113.14681189547798,
        width: 28,
        height: 32
      },
    ]
  },

  // onLoad: function () {
  //   var that = this;

  //   wx.getLocation({
  //     type: "wgs84",
  //     success: function (res) {
  //       var latitude = res.latitude;
  //       var longitude = res.longitude;
  //       console.log("当前位置的经纬度为：", res.latitude, res.longitude);
  //       that.setData({
  //         latitude: res.latitude,
  //         longitude: res.longitude,

  //       })
  //     }
  //   })
  // },
  onReady: function () {

  },

  /**
   * 地图放大缩小事件触发
   * @param {*} e 
   */
  bindregionchange(e) {
    console.log('=bindregiοnchange=', e)
  },

  /**
   * 点击地图事件，有经纬度信息返回
   * @param {*} e 
   */
  bindtapMap(e) {
    console.log('=bindtapMap=', e)
  }
})
